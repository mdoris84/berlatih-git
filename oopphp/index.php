<?php
require_once('animal.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Name :" . $sheep->jenis . "<br>"; // "shaun"
echo "Legs :" . $sheep->legs . "<br>";// 4
echo "Cold Blooded :" . $sheep->cold_blooded . "<br> <br>"; // "no"

$sungokong = new Ape("kera sakti");

echo "Name :" . $sungokong->jenis . "<br>"; // "kera sakti"
echo "Legs :" . $sungokong->legs . "<br>";// 4
echo "Cold Blooded :" . $sungokong->cold_blooded . "<br> <br>"; // "no"
echo $sungokong->yell() // "Auooo"

$kodok = new Frog("buduk");
echo "Name :" . $kodok->jenis . "<br>"; // "buduk"
echo "Legs :" . $kodok->legs . "<br>";// 4
echo "Cold Blooded :" . $kodok->cold_blooded . "<br>"; // "no"
echo $kodok->jump() ; // "hop hop"

?>